﻿using System.ComponentModel.DataAnnotations;

namespace baiTapThayGiao2.Models
{
    public class Student
    {
        [Key]
        public long StudentId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Address { get; set; }

        // Relationships
        public List<Exam> Exam { get; set; }
    }
}
