﻿using System.ComponentModel.DataAnnotations;

namespace baiTapThayGiao2.Models
{
    public class Subject
    {
        [Key]
        public int SubjectId { get; set; }
        //[MinLength(8)]
        //[MaxLength(150)]
        [Required]
        public string SubjectName { get; set; }
        //[MinLength(2)]
        //[MaxLength(20)]
        [Required]
        public string SubjectCode { get; set; }
        //[StringLength(500)]
        [Required]
        public string Description { get; set; }
        [Required]
        //[DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Required]
        //[DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        // Relationships
        public List<Exam> Exam { get; set; }
    }
}
