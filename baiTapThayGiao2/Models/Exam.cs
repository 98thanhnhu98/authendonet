﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace baiTapThayGiao2.Models
{
    public class Exam
    {
        [Key]
        public long ExamId { get; set; }

        [Required]
        //[Range(0, 100)]
        public int Score { get; set; }

        public long StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        public int SubjectId { get; set; }
        [ForeignKey("SubjectId")]
        public Subject Subject { get; set; }
    }
}
